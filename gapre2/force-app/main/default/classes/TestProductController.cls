@isTest
public class TestProductController {

    @testSetup
    static void createProducts() {
        List<Product__c> products = new List<Product__c>();

        products.add(new Product__c(
                        Name='Sample Bike 1',
                        MSRP__c=1000,
                        Category__c='Mountain',
                        Level__c='Beginner',
                        Material__c='Carbon'));

        products.add(new Product__c(
                        Name='Sample Bike 2',
                        MSRP__c=1200,
                        Category__c='Mountain',
                        Level__c='Beginner',
                        Material__c='Carbon'));

        insert products;

        Topic t = new Topic(Name='test');
        insert t;

        TopicAssignment ta = new TopicAssignment();
        ta.EntityId = products[0].Id;
        ta.TopicId = t.Id;
        insert ta;


    }

    @isTest
    static void testGetProducts() {
        Topic t = [SELECT Id FROM Topic WHERE Name = 'test' LIMIT 1];
        String filters = '{"searchKey":"Sample", "maxPrice":2000, "categories": ["Mountain"], "levels": ["Beginner"], "materials":["Carbon"],"topics":["' + t.Id + '"]}, 1';
        PagedResult result = ProductController.getProducts(filters, 1);
        System.assertEquals(result.records.size(), 1);
    }

    @isTest
    static void testGetSimilarProducts() {
        String filters = '{"searchKey":"Sample", "maxPrice":2000, "categories": ["Mountain"], "levels": ["Beginner"], "materials":["Carbon"]}, 1';
        PagedResult result = ProductController.getProducts(filters, 1);
        Product__c productToCompare = (Product__c)result.records[0];
        Product__c[] products = ProductController.getSimilarProducts(productToCompare.Id, null);
        System.assertEquals(products.size(), 1);
    }

    @isTest
    static void testGetSimilarProductsByTopic() {
        Topic t = [SELECT Id,Name FROM Topic WHERE Name = 'test' LIMIT 1];
        Product__c[] products = ProductController.getSimilarProductsByTopic(t.Id);
        System.assertEquals(products.size(), 1);
    }

     @isTest
    static void testGetProductTopics() {
        String topicsMapJSON = ProductController.getProductTopics(); 
        Map<String,Object> topicsMap = (Map<String,Object>)System.JSON.deserializeUntyped(topicsMapJSON);
        System.debug(topicsMap);
        System.assertEquals(1, topicsMap.keySet().size());
    }

    @isTest
    static void testSearchProducts() {
        Topic t = [SELECT Id FROM Topic WHERE Name = 'test' LIMIT 1];
        List<Product__c> prdList = [SELECT Id FROM Product__c WHERE Name LIKE 'Sample%'];
        List<Id> prdIdList = new List<Id>();
        for(Product__c p : prdList)
        {
            prdIdList.add(p.Id);
        }
        System.Test.setFixedSearchResults(prdIdList);
        String filters = '{"searchKey":"Sample", "maxPrice":-1, "categories": ["Mountain"], "levels": ["Beginner"], "materials":["Carbon"],"topics":["' + t.Id + '"]}';
        PagedResult result = ProductController.searchProducts(filters,5);
        System.assertEquals(result.records.size(), 1);
    }

}