public with sharing class ProductController {

    static Integer PAGE_SIZE = 9;

    public class Filters {
        public String searchKey { get;set; }

        public Decimal maxPrice { get;set; }

        public String[] categories { get;set; }

        public String[] materials { get;set; }

        public String[] levels { get;set; }

        public String[] topics { get;set; }
    }

    @AuraEnabled(Cacheable=true)
    public static PagedResult getProducts(String filters, Integer pageNumber) {
        String key, whereClause = '';
        Decimal maxPrice;
        String[] categories, materials, levels, topics, criteria = new String[]{};
        if (!String.isEmpty(filters)) {
            Filters productFilters = (Filters) JSON.deserializeStrict(filters, ProductController.Filters.class);
            maxPrice = productFilters.maxPrice;
            materials = productFilters.materials;
            levels = productFilters.levels;
            topics = productFilters.topics;
            if (!String.isEmpty(productFilters.searchKey)) {
                key = '%' + productFilters.searchKey + '%';
                criteria.add('Name LIKE :key');
            }
            if (productFilters.maxPrice >= 0) {
                maxPrice = productFilters.maxPrice;
                criteria.add('MSRP__c <= :maxPrice');
            }
            if (productFilters.categories != null) {
                categories = productFilters.categories;
                criteria.add('Category__c IN :categories');
            }                      
            if (productFilters.levels != null) {
                levels = productFilters.levels;
                criteria.add('Level__c IN :levels');
            }                      
            if (productFilters.materials != null) {
                materials = productFilters.materials;
                criteria.add('Material__c IN :materials');
            }
            if (productFilters.topics != null && productFilters.topics.size() > 0) {
                topics = productFilters.topics;
                String topicsFilterQuery = 'Id IN (SELECT EntityId FROM TopicAssignment WHERE NetworkId = :networkId AND ';
                topicsFilterQuery += 'TopicId IN :topics AND EntityType = \'Product__c\')';
                criteria.add(topicsFilterQuery);
            }
            if (criteria.size() > 0) {
                whereClause = 'WHERE ' + String.join( criteria, ' AND ' );
            }                      
        }
        Integer pageSize = ProductController.PAGE_SIZE;
        Integer offset = (pageNumber - 1) * pageSize;
        String networkId = System.Network.getNetworkId();
        PagedResult result =  new PagedResult();
        result.pageSize = pageSize;
        result.pageNumber = pageNumber;
        result.totalItemCount = Database.countQuery('SELECT count() FROM Product__c ' + whereClause);
        result.records = Database.query('SELECT Id, Name, MSRP__c, Description__c, Category__c, Level__c, Picture_URL__c, Material__c, (SELECT Topic.Id,Topic.Name FROM TopicAssignments WHERE NetworkId = :networkId ORDER BY Topic.Name ASC) FROM Product__c ' + whereClause + ' ORDER BY Name LIMIT :pageSize OFFSET :offset');
        return result;
    }

    @AuraEnabled(Cacheable=true)
    public static Product__c[] getSimilarProducts(Id productId, Id familyId) {
        return [SELECT Id, Name, MSRP__c, Description__c, Category__c, Level__c, Picture_URL__c, Material__c FROM Product__c WHERE Product_Family__c=:familyId AND Id!=:productId];
    }

    @AuraEnabled(Cacheable=true)
    public static Product__c[] getSimilarProductsByTopic(Id topicId) {

        String networkId = System.Network.getNetworkId();

        List<TopicAssignment> topicAssignmentList = [SELECT EntityId
                                    FROM TopicAssignment WHERE TopicId = :topicId AND EntityType = 'Product__c' AND NetworkId = :networkId];
        List<Id> productListIds = new List<Id>();

        for(TopicAssignment ta : topicAssignmentList)
        {
            productListIds.add(ta.EntityId);
        }

        return [SELECT Id, Name, MSRP__c, Description__c, Category__c, Level__c, Picture_URL__c, Material__c FROM Product__c WHERE Id IN :productListIds];
    
    }

    @AuraEnabled(Cacheable=true)
    public static String getProductTopics() {

        Map<String,String> topicsMap = new Map<String,String>();
        String networkId = System.Network.getNetworkId();

        AggregateResult[] groupedResults = [SELECT TopicId FROM TopicAssignment 
        WHERE EntityType = 'Product__c' AND NetworkId = :networkId
        GROUP BY TopicId];

        Set<Id> topicIdSet = new Set<Id>();

        for (AggregateResult ar : groupedResults)  {
            topicIdSet.add((Id)ar.get('TopicId'));
        }

        List<Topic> topicList = [Select Id, Name FROM Topic WHERE Id IN :topicIdSet];
        for(Topic t : topicList)
        {
            topicsMap.put(t.Id,t.Name);
        }

        return System.JSON.serialize(topicsMap);
    }

    @AuraEnabled(Cacheable=true)
    public static PagedResult searchProducts(String filters, Integer maxResultCount) {
        PagedResult result =  new PagedResult();
        result.pageSize = 0;
        result.pageNumber = 0;
        result.totalItemCount = 0;
        result.records = new List<Product__c>();

        Integer resultCountToQuery = maxResultCount + 1;

        String key, whereClause = '';
        Decimal maxPrice;
        String[] categories, materials, levels, topics, criteria = new String[]{};
        String networkId = System.Network.getNetworkId();
        if (!String.isEmpty(filters)) {
            Filters productFilters = (Filters) JSON.deserializeStrict(filters, ProductController.Filters.class);
            topics = productFilters.topics;

            if(String.isEmpty(productFilters.searchKey) || productFilters.searchKey.length() < 2)
            {
                return result;
            }

            
            if (!String.isEmpty(productFilters.searchKey)) {
                key = productFilters.searchKey;
            }
            if (productFilters.maxPrice >= 0) {
                maxPrice = productFilters.maxPrice;
                criteria.add('MSRP__c <= :maxPrice');
            }
            if (productFilters.categories != null) {
                categories = productFilters.categories;
                criteria.add('Category__c IN :categories');
            }                      
            if (productFilters.levels != null) {
                levels = productFilters.levels;
                criteria.add('Level__c IN :levels');
            }                      
            if (productFilters.materials != null) {
                materials = productFilters.materials;
                criteria.add('Material__c IN :materials');
            }
            if (productFilters.topics != null && productFilters.topics.size() > 0) {
                topics = productFilters.topics;
                System.debug(topics);
                String topicsFilterQuery = 'Id IN (SELECT EntityId FROM TopicAssignment WHERE NetworkId = :networkId AND ';
                topicsFilterQuery += 'TopicId IN :topics AND EntityType = \'Product__c\')';
                criteria.add(topicsFilterQuery);
            }
            if (criteria.size() > 0) {
                whereClause = String.join( criteria, ' AND ' );
            }                  

            String query = 'FIND :key IN ALL FIELDS RETURNING ';
            query += 'Product__c(Id, Name, MSRP__c, Description__c, ';
            query += 'Category__c, Level__c, Picture_URL__c, Material__c ';
            if(!String.isEmpty(whereClause))
            {
                query += 'WHERE ' + whereClause;
            }
            query += ' ORDER BY Name LIMIT ' + resultCountToQuery + ')';

            System.debug(query);

            List<List<SObject>> searchResults = System.Search.Query(query); 

            result.records = (searchResults != null && searchResults.size() > 0) ? (List<Product__c>)searchResults[0] : null;

            if(result.records != null && result.records.size() == 0)
            {
                result.totalItemCount = 0;
            }
            else if(result.records != null && result.records.size() > maxResultCount)
            {
                result.totalItemCount = result.records.size();
                result.records.remove(result.records.size()-1);
            }
            else if(result.records != null && result.records.size() <= maxResultCount)
            {
                result.totalItemCount = result.records.size();
            }

            if(result.records != null && result.records.size() > 0)
            {
                List<Product__c> prdList = (List<Product__c>)result.records;
                result.records = Database.query('SELECT Id, Name, MSRP__c, Description__c, Category__c, Level__c, Picture_URL__c, Material__c, (SELECT Topic.Id,Topic.Name FROM TopicAssignments WHERE NetworkId = :networkId ORDER BY Topic.Name ASC) FROM Product__c WHERE Id IN :prdList ORDER BY Name LIMIT 6');
            }

            return result;

        }
        else {
            return result;
        }
        //result.records = Database.query('SELECT Id, Name, MSRP__c, Description__c, Category__c, Level__c, Picture_URL__c, Material__c, (SELECT Topic.Id,Topic.Name FROM TopicAssignments WHERE NetworkId = :networkId ORDER BY Topic.Name ASC) FROM Product__c ' + whereClause + ' ORDER BY Name LIMIT :pageSize OFFSET :offset');
        
    }

}