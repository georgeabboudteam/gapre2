public with sharing class NavigationItemsController {
    public NavigationItemsController() {}

    @AuraEnabled(cacheable=true)
    public static List<NavigationMenuItem> getNavigationItems(String menuSetDevName, String publishStatus) {
        List<NavigationLinkSet> linkSets = [
            SELECT Id 
            FROM NavigationLinkSet
            WHERE developerName = :menuSetDevName
        ];
        Id linkSetId = linkSets.get(0).Id;
        return [SELECT Label, Target
            FROM NavigationMenuItem
            WHERE NavigationLinkSetId = :linkSetId
            AND Status = :publishStatus 
            ORDER BY Position
        ];
    }

}