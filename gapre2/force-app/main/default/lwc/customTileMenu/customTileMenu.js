import { LightningElement, api, track} from 'lwc';

export default class CustomTileMenu extends LightningElement {

    @api tilesJSON = "[{\"imageUrl\":\"/ebikes/file-asset/tileMenuBike?v=1\",\"linkUrl\":\"https://www.google.com\",\"linkText\":\"View More\",\"titleText\":\"Create Case\"}, {\"imageUrl\":\"/ebikes/file-asset/tileMenuBike?v=1\",\"linkUrl\":\"https://www.google.com\",\"linkText\":\"View More\",\"titleText\":\"Create Case\"},{\"imageUrl\":\"/ebikes/file-asset/tileMenuBike?v=1\",\"linkUrl\":\"https://www.google.com\",\"linkText\":\"View More\",\"titleText\":\"Create Case\"}, {\"imageUrl\":\"/ebikes/file-asset/tileMenuBike?v=1\",\"linkUrl\":\"https://www.google.com\",\"linkText\":\"View More\",\"titleText\":\"Create Case\"}]";

    @api tilesOnRow = '2';

    @api showFullImage = false;

    @api opacity = 5;

    @track tileList;

    @track tileSize;

    get layoutItemClasses() {
        if(this.showFullImage)
        {
            return 'height100';
        }
        
        return 'height50';
        
    }


    connectedCallback(){
        if(this.tileList === null || this.tileList === undefined || this.tileList.length === 0)
        {
            this.tileList = JSON.parse(this.tilesJSON);
            let tileOnRowInt = parseInt(this.tilesOnRow,10);
            for(let i=0;i<this.tileList.length;i++)
            {
                this.tileList[i].id = i;
                
                if(this.tileList[i].imageStyle !== undefined && this.tileList[i].imageStyle !== null &&
                    this.tileList[i].imageStyle.trim() !== '')
                {
                    this.tileList[i].imageStyle += 'background-image: url(' + this.tileList[i].imageUrl + ');';
                }
                else
                {
                    this.tileList[i].imageStyle = 'background-image: url(' + this.tileList[i].imageUrl + ');';
                }

                if(this.tileList[i].opacity === undefined || this.tileList[i].opacity === null || 
                    this.tileList[i].opacity < 0 || this.tileList[i].opacity > 10)
                {
                    this.tileList[i].overlayStyle = 'opacity: ' + this.opacity / 10 + ';';
                }
                else
                {
                    this.tileList[i].overlayStyle = 'opacity: ' + parseInt(this.tileList[i].opacity, 10) / 10 + ';';
                }

                if(this.tileList[i].overlayBackgroundColor !== undefined && this.tileList[i].overlayBackgroundColor !== null && 
                    this.tileList[i].overlayBackgroundColor.trim() !== '')
                {
                    this.tileList[i].overlayStyle += 'background-color: ' + this.tileList[i].overlayBackgroundColor + ';';
                }

                if((i+1)%tileOnRowInt === 1)
                {
                    this.tileList[i].tilePaddingClass = 'slds-p-right_medium slds-m-vertical_medium no-horizontal-padding';
                }
                else if((i+1)%tileOnRowInt === 0)
                {
                    this.tileList[i].tilePaddingClass = 'slds-p-left_medium slds-m-vertical_medium no-horizontal-padding';
                }
                else
                {
                    this.tileList[i].tilePaddingClass = 'slds-p-horizontal_medium slds-m-vertical_medium no-horizontal-padding';
                }

                if(this.tileList[i].openInNewTab !== undefined && this.tileList[i].openInNewTab !== null &&
                    this.tileList[i].openInNewTab === true)
                {
                    this.tileList[i].hreftarget = '_blank';
                }
                else
                {
                    this.tileList[i].hreftarget = '';
                }

            }
        }
        
        let tmpTilesOnRow;
        try { 
            tmpTilesOnRow = parseInt(this.tilesOnRow, 10);
        } catch(err){
            tmpTilesOnRow = 4;
        }
        tmpTilesOnRow = (tmpTilesOnRow === null || tmpTilesOnRow === undefined) ? 4 : tmpTilesOnRow;
        tmpTilesOnRow = (tmpTilesOnRow > 6) ? 6 : tmpTilesOnRow;
        tmpTilesOnRow = (tmpTilesOnRow < 1) ? 1 : tmpTilesOnRow;
        this.tileSize = Math.round(12 / tmpTilesOnRow);
        
    }

}