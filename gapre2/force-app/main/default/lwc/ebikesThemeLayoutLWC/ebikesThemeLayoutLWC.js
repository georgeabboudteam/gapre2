import { LightningElement, api } from 'lwc';

export default class EbikesThemeLayoutLWC extends LightningElement {

    @api searchPageBaseUrl = '/ebikestalon/s/global-search/';
    @api searchPlaceholder = 'Search Products...';
    @api navigationMenuDevName = 'Default_Navigation';


    handleNavigateToHome(event) {
       // eslint-disable-next-line no-console
       console.log(event);
    }

}