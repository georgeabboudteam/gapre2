import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import TOPIC_OBJECT from '@salesforce/schema/Topic';

export default class AssignedTopics extends NavigationMixin(LightningElement) {

    _assignedTopics;

    @api
    get assignedTopics() {
        return this._assignedTopics;
    }
    set assignedTopics(value) {
        this._assignedTopics = value;
    }
    
    handleClick(e) {
        e.preventDefault();
        let topicid = e.currentTarget.dataset.topicid;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: topicid,
                objectApiName: TOPIC_OBJECT.objectApiName,
                actionName: 'view'
            }
        });

    }

}