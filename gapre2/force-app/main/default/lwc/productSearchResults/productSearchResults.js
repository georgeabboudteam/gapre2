import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';


/** getProducts() method in ProductController Apex class */
import searchProducts from '@salesforce/apex/ProductController.searchProducts';

/**
 * Container component that loads and displays a list of Product__c records.
 */
export default class ProductSearchResults extends NavigationMixin(LightningElement) {

    @api searchString;
    @api topicId;
    @api displayType = 'Tile'; //'List' or 'Tile'
    
    /** All available Product__c[]. */
    @track products = [];


    /** The total number of items matching the selection. */
    @track totalItemCount = '0';

    maxResultCount = 5;

    @track searchStringTooShort = false;

    get cardClasses()
    {
        return (this.isTile) ? 'slds-card slds-p-around_x-small border-radius-none border-none':'slds-p-around_x-small border-radius-none border-none';
    }
    set cardClasses(value)
    {
        this._cardClasses = value;
    }

    get isTile()
    {
        return this.displayType === 'Tile';
    }
    set isTile(value)
    {
        this._isTile = value;
    }

    /** JSON.stringified version of filters to pass to apex */
    get filters()
    {
        let filterObject = {};
        filterObject.searchKey = this.searchString;
        
        if(this.searchString !== null && this.searchString !== undefined && this.searchString.length < 2)
        {
            this.searchStringTooShort = true;
        }

        if(this.topicId !== null && this.topicId !== undefined)
        {
            filterObject.topics = [];
            filterObject.topics.push(this.topicId);
        }
        else
        {
            filterObject.topics = null;
        }

        filterObject.maxPrice = -1;
        filterObject.materials = null;
        filterObject.levels = null;
        filterObject.categories = null;

        return JSON.stringify(filterObject);
    }
    set filters(value)
    {
        this._filters = value;
    }

    @wire(CurrentPageReference) pageRef;

    /**
     * Load the list of available products.
     */
    @wire(searchProducts, { filters: '$filters', maxResultCount: '$maxResultCount' })
    wiredProducts({ error, data }) {
        if (data) {

            if(data !== null && data !== undefined
                && data.records !== null && data.records !== undefined
                && data.totalItemCount !== null && data.totalItemCount !== null
                && data.totalItemCount === (this.maxResultCount + 1) && data.records.length === this.maxResultCount )
            {
                this.totalItemCount = this.maxResultCount + '+';
            }
            else if(data !== null && data !== undefined
                && data.records !== null && data.records !== undefined
                && data.totalItemCount !== null && data.totalItemCount !== null
                && data.totalItemCount === data.records.length )
            {
                this.totalItemCount = data.records.length + '';
            }

            this.products.data = data;

        } else if (error) {
            this.totalItemCount = '0';
            this.products.error = error;
        }

        return null;

    }

    handleClick(e) {
        e.preventDefault();
        let recordid = e.currentTarget.dataset.recordid;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: recordid,
                objectApiName: "Product__c",
                actionName: 'view'
            }
        });

    }

}