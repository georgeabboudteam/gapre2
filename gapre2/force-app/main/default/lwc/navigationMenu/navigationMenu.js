import { LightningElement, api, wire, track } from 'lwc';
import getNavigationItems from '@salesforce/apex/NavigationItemsController.getNavigationItems';
import { NavigationMixin } from 'lightning/navigation';

export default class NavigationMenu extends NavigationMixin(LightningElement) {

    /**
     * the developer name of the NavigationMenuLinkSet exposed by the .js-meta.xml
     */
    @api menuSetDevName;
    /**
     * the menu items when fetched by the NavigationItemsController
     */
    @track menuItems;
    /**
     * the error if it occurs
     */
    @track error;

    /**
     * the publish status of the communitiy, needed in order to determine from which schema to 
     * fetch the NavigationMenuItems from
     */
    publishStatus;

    /**
     * We use wire and pass it the controller and the arguments that the controller needs. 
     */
    @wire(getNavigationItems, { 
        menuSetDevName: '$menuSetDevName',
        publishStatus: '$publishStatus'
    })
    wiredMenuItems({error, data}) {
        if (data && !this.menuItems) {
            this.menuItems = data.map((item, index) => {
                return {
                    target: item.Target,
                    id: index,
                    label: item.Label
                }
            });
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.wiredMenuItems = null;
        }
    }

    connectedCallback() {
        // Determine the publish status based on the current location.
        const { location } = window;
        const { href } = location;
        this.publishStatus = href.includes('--preview.') || href.includes('--live.') ? 'Draft' : 'Live';
    }

    handleClickHome() {
        // use the NavigationMixin from lightning/navigation to perform
        // the navigation. 
        const pageReference = {
            type: 'comm__namedPage',
            attributes: { 
                pageName: 'home'
             }
        };

        this[NavigationMixin.Navigate](pageReference);
    }

}