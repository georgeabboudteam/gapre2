import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class NavigationItem extends NavigationMixin(LightningElement) {

    // the navigationMenuItem from the apex controller, 
    // contains a label and a target.
    @api item = {};

    handleClick() {
        // use the NavigationMixin from lightning/navigation to perform
        // the navigation. 
        const pageReference = {
            type: 'comm__namedPage',
            attributes: { 
                pageName: this.item.target.substr(1)
             }
        };

        this[NavigationMixin.Navigate](pageReference);
    }

}