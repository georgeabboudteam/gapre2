import { LightningElement, api, wire } from 'lwc';
import getSimilarProductsByTopic from '@salesforce/apex/ProductController.getSimilarProductsByTopic';


export default class SimilarProducts extends LightningElement {
    @api topicId;

    // Track changes to the Product_Family__c field that could be made in other components.
    // If Product_Family__c is updated in another component, getSimilarProducts
    // is automatically re-invoked with the new this.familyId parameter

    @wire(getSimilarProductsByTopic, {
        topicId: '$topicId'
    })
    similarProducts;

    get errors() {
        const errors = [this.similarProducts.error].filter(
            error => error
        );
        return errors.length ? errors : undefined;
    }
}