import { LightningElement, track, api, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import searchProducts from '@salesforce/apex/ProductController.searchProducts';

const DELAY = 300;

export default class ProductSearch extends NavigationMixin(LightningElement) {
    @track queryTerm;
    @api searchPageName = "/ebikes/s/global-search/global-search";
    @api searchPlaceholder = "Search Products...";
    @track searchString;
    @track products = [];
    @track searchStringTooShort = false;

    maxResultCount = 5;
    
    get filters()
    {
        let filterObject = {};
        filterObject.searchKey = this.searchString;
        
        if(this.searchString !== null && this.searchString !== undefined && this.searchString.length < 2)
        {
            this.searchStringTooShort = true;
        }

        filterObject.topics = null;
        

        filterObject.maxPrice = -1;
        filterObject.materials = null;
        filterObject.levels = null;
        filterObject.categories = null;

        return JSON.stringify(filterObject);
    }

    set filters(value)
    {
        this._filters = value;
    }

    get noSearchResults()
    {
        return (this.queryTerm !== null && this.queryTerm !== undefined && this.queryTerm.length > 0 
            && this.products.data.records.length === 0);
    }

    @wire(searchProducts, { filters: '$filters', maxResultCount: (this !== null && this !== undefined && this.maxResultCount !== null && this.maxResultCount !== undefined) ? parseInt(JSON.parse(JSON.stringify(this.maxResultCount))+'',10) : parseInt(JSON.parse(JSON.stringify('5')),10) })
    wiredProducts({ error, data }) {
        if (data) {
            this.products.data = data;
        }
        else if (error) {
            this.products.error = error;
        }
        return null;
    }

    connectedCallback() {
        let currUrl = window.location.href;
        if( (this.queryTerm === null || this.queryTerm === undefined || this.queryTerm === '') 
        && currUrl.indexOf(this.searchPageName) > 0)
        {
            //this.queryTerm = decodeURI(currUrl.substr(currUrl.lastIndexOf('/') + 1));
        }
    }

    handleKeyUp(evt) {
        const isEnterKey = evt.keyCode === 13;
        const tmpQueryTerm = evt.target.value;
        
        if(!isEnterKey && tmpQueryTerm && tmpQueryTerm.trim().length > 2)
        {
            this.queryTerm = tmpQueryTerm;
            window.clearTimeout(this.delayTimeout);
            // eslint-disable-next-line @lwc/lwc/no-async-operation
            this.delayTimeout = setTimeout(() => {
                this.searchStringTooShort = false;
                this.searchString = tmpQueryTerm;
                let filterObj = JSON.parse(this.filters);
                filterObj.searchKey = tmpQueryTerm;
                this.filters = JSON.stringify(filterObj);
            }, DELAY);
            
        }
        else if(!isEnterKey && tmpQueryTerm && tmpQueryTerm.trim().length < 3)
        {
            this.queryTerm = tmpQueryTerm;
            this.searchStringTooShort = true;
        }
        else
        {
            this.resetSearch();
        }

    }

    handleKeyUpEnter(evt) {
        const isEnterKey = evt.keyCode === 13;
        const tmpQueryTerm = evt.target.value;

        if (isEnterKey && tmpQueryTerm && tmpQueryTerm.trim().length > 2) {
            
            let searchPageUrl = this.searchPageName + tmpQueryTerm;
            window.location.href = searchPageUrl;

        }
    }

    handleClick(e) {
        e.preventDefault();
        let recordid = e.currentTarget.dataset.recordid;
        
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: recordid,
                objectApiName: "Product__c",
                actionName: 'view'
            }
        });
        this.resetSearch();
    }

    resetSearch() {
        this.searchStringTooShort = false;
        this.queryTerm = null;
        this.searchString = null;
        this.products = [];
        let filterObj = JSON.parse(this.filters);
        filterObj.searchKey = null;
        this.filters = JSON.stringify(filterObj);
    }


}