import { LightningElement, wire, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import CATEGORY_FIELD from '@salesforce/schema/Product__c.Category__c';
import LEVEL_FIELD from '@salesforce/schema/Product__c.Level__c';
import MATERIAL_FIELD from '@salesforce/schema/Product__c.Material__c';
import getProductTopics from '@salesforce/apex/ProductController.getProductTopics';

/** Pub-sub mechanism for sibling component communication. */
import { fireEvent } from 'c/pubsub';

/** The delay used when debouncing event handlers before firing the event. */
const DELAY = 350;

/**
 * Displays a filter panel to search for Product__c[].
 */
export default class ProductFilter extends LightningElement {

    @track productTopics = [];

    searchKey = '';
    maxPrice = 10000;

    filters = {
        categories: null,
        levels: null,
        materials: null,
        topics: null,
        searchKey: '',
        maxPrice: 10000
    };

    @wire(CurrentPageReference) pageRef;

    @wire(getPicklistValues, {
        recordTypeId: '012000000000000AAA',
        fieldApiName: CATEGORY_FIELD
    })
    categories;

    @wire(getPicklistValues, {
        recordTypeId: '012000000000000AAA',
        fieldApiName: LEVEL_FIELD
    })
    levels;

    @wire(getPicklistValues, {
        recordTypeId: '012000000000000AAA',
        fieldApiName: MATERIAL_FIELD
    })
    materials;

    @wire(getProductTopics)
    productTopicsJSON(result) {
        var response;
        if (result.data) {

            response = JSON.parse(result.data);
            //this.productTopics = response;
            
            for (let key in response) {
                if (response.hasOwnProperty(key)) {  
                let obj = {};
                obj.topicId = key;
                obj.topicName = response[key];
                this.productTopics.push(obj);
                }
            }

        } else if (result.error) {
            //debug(this.debugOn,error);
        }
    }


    handleSearchKeyChange(event) {
        this.filters.searchKey = event.target.value;
        this.delayedFireFilterChangeEvent();
    }

    handleMaxPriceChange(event) {
        const maxPrice = event.target.value;
        this.filters.maxPrice = maxPrice;
        this.delayedFireFilterChangeEvent();
    }

    handleCheckboxChange(event) {

        this.buildFiltersFromUI('categories');
        this.buildFiltersFromUI('levels');
        this.buildFiltersFromUI('materials');
        this.buildFiltersFromUI('topics');

        

        /*if(event !== undefined && event !== null)
        {
            const value = event.target.dataset.value;
            const filterArray = this.filters[event.target.dataset.filter];
            if (event.target.checked) {
                if (!filterArray.includes(value)) {
                    filterArray.push(value);
                }
            } else {
                this.filters[event.target.dataset.filter] = filterArray.filter(
                    item => item !== value
                );
            }
        }*/
        fireEvent(this.pageRef, 'filterChange', this.filters);
    }

    handleSelectAllClick(event){
        event.preventDefault();
        let filter = event.target.dataset.filter;
        let operation = event.target.dataset.operation;
        let lightningInputs = this.template.querySelectorAll('lightning-input[data-filter='+filter+']');
        for(let i=0;i<lightningInputs.length;i++)
        {
            lightningInputs[i].checked = (operation === 'select') ? true : false;
        }
        
        this.handleCheckboxChange(null);
    }

    buildFiltersFromUI(filter){
        let lightningInputs = this.template.querySelectorAll('lightning-input[data-filter='+filter+']');
        this.filters[filter] = [];
        
        for(let i=0;i<lightningInputs.length;i++)
        {
            if(lightningInputs[i].checked)
            {
                this.filters[filter].push(lightningInputs[i].dataset.value);
            }
        }
    }

    delayedFireFilterChangeEvent() {
        // Debouncing this method: Do not actually fire the event as long as this function is
        // being called within a delay of DELAY. This is to avoid a very large number of Apex
        // method calls in components listening to this event.
        window.clearTimeout(this.delayTimeout);
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            fireEvent(this.pageRef, 'filterChange', this.filters);
        }, DELAY);
    }
}